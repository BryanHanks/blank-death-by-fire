# _blank Death by Fire
<pre>
       __       ___                    __
       /\ \     /\_ \                  /\ \
       \ \ \____\//\ \      __      ___\ \ \/'\
        \ \ '__`\ \ \ \   /'__`\  /' _ `\ \ , <
         \ \ \L\ \ \_\ \_/\ \L\.\_/\ \/\ \ \ \\`\
          \ \_,__/ /\____\ \__/.\_\ \_\ \_\ \_\ \_\
    _______\/___/  \/____/\/__/\/_/\/_/\/_/\/_/\/_/
   /\______\
   \/______/

                                                          (  .      )
                                                      )           (              )
                                                            .  '   .   '  .  '  .
                                                   (    , )       (.   )  (   ',    )
                                                    .' ) ( . )    ,  ( ,     )   ( .
                                                 ). , ( .   (  ) ( , ')  .' (  ,    )
                                                (_,) . ), ) _) _,')  (, ) '. )  ,. (' )
     ____                     __    __          __                  ____
    /\  _`\                  /\ \__/\ \        /\ \                /\  _`\   __
    \ \ \/\ \     __     __  \ \ ,_\ \ \___    \ \ \____  __  __   \ \ \L\_\/\_\  _ __    __
     \ \ \ \ \  /'__`\ /'__`\ \ \ \/\ \  _ `\   \ \ '__`\/\ \/\ \   \ \  _\/\/\ \/\`'__\/'__`\
      \ \ \_\ \/\  __//\ \L\.\_\ \ \_\ \ \ \ \   \ \ \L\ \ \ \_\ \   \ \ \/  \ \ \ \ \//\  __/
       \ \____/\ \____\ \__/.\_\\ \__\\ \_\ \_\   \ \_,__/\/`____ \   \ \_\   \ \_\ \_\\ \____\
        \/___/  \/____/\/__/\/_/ \/__/ \/_/\/_/    \/___/  `/___/> \   \/_/    \/_/\/_/ \/____/
                                                              /\___/
                                                              \/__/
</pre>

**Version:** 0.98

**Author:** bryanh

**Date:** 2020-05-12


## Background

As an antidote to all the lazy, no-nothing web "developers" who think it is
okay for a link to open a new tab, this extension is here for you.

I wrote this extension because the one I used prior ("Death to Blank") seems to
have disappeared, and I finally got sick of having new tabs open all the time
on a site I have to use for work.

_Fear leads to anger, anger leads to hate and hate leads to development._

If you absolutely must open a link in another window/tab, then there are
keyboard shortcuts that will enable you to do so, typically holding down
ALT/Command CTRL/Option when clicking the link. Check your browser's
documentation for details.

## Caveat
Note that this extension will NOT work on links that use JavaScript or
some other nefarious method for opening a new tab. I figure if we can fix
80% of the trash out there, we're doing well!

This extension might not work on mobile. I haven't noticed the problem on mobile,
so maybe we got lucky and the mobile browser developers automatically ignore the
open tab functionality?

## Credits
* Skull icon by [Alpár-Etele Méde](https://thoseicons.com/)
* The kind soul who made the Death to Blank extesion all those years ago

## License
This work is licensed under the [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

## Possible Future Enhancements


- Option to change the link color of 'fixed' links
- Option to add a skull or fire emoji either leading or trailing the 'fixed' links
- Option to "whitelist" certain sites to allow links to open new tabs.
