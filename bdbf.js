/**
  bdbf.js

  _blank Death by Fire
  Remove stupid atteributes that open links in a new tab
//*/


var xresults = document.evaluate(
  "//a[@target='_blank' or @target='_new']",
  document,
  null,
  XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
  null
);


for( var i = 0; i < xresults.snapshotLength; i++ ) {
  //*console.log( 'Pull out the target [' + i + '][' + xresults.snapshotItem(i).target + ']' );//*/
  xresults.snapshotItem(i).removeAttribute('target');
}

